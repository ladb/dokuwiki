FROM istepanov/dokuwiki
MAINTAINER Ilya Stepanov <dev@ilyastepanov.com>

# this is an example Dockerfile that demonstrates how to add Dokuwiki plugins to istepanov/dokuwiki image

RUN apt-get update && \
    apt-get install -y unzip php5-gd php5-sqlite && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY plugins.sh /root/plugins.sh

RUN chmod +x /root/plugins.sh && /root/plugins.sh

COPY htaccess.txt /var/www/.htaccess
