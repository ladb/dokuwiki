#!/bin/bash

unzip-strip() (
    local zip=$1
    local dest=${2:-.}
    local temp=$(mktemp -d) && unzip -d "$temp" "$zip" && mkdir -p "$dest" &&
    shopt -s dotglob && local f=("$temp"/*) &&
    if (( ${#f[@]} == 1 )) && [[ -d "${f[0]}" ]] ; then
        mv "$temp"/*/* "$dest"
    else
        mv "$temp"/* "$dest"
    fi && rmdir "$temp"/* "$temp"
)
cd /tmp

# set timezone to LA
  cp /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

# add bootstrap3 theme
  curl -o stable.zip -L "https://github.com/LotarProject/dokuwiki-template-bootstrap3/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/tpl/bootstrap3 && \
  rm stable.zip

# add bootstrap wrapper plugin
  curl -o stable.zip -L "https://github.com/LotarProject/dokuwiki-plugin-bootswrapper/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/bootswrapper && \
  rm stable.zip

# add icons plugin
  curl -o stable.zip -L "https://github.com/LotarProject/dokuwiki-plugin-icons/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/icons && \
  rm stable.zip

# add font-awesome
  curl -o stable.zip -L "http://fontawesome.io/assets/font-awesome-4.5.0.zip" && \
  mkdir -p /var/www/assets && \
  unzip-strip stable.zip /var/www/assets/font-awesome && \
  rm stable.zip

# add todo plugin
  curl -o stable.zip -L "https://github.com/leibler/dokuwiki-plugin-todo/archive/stable.zip" && \
  unzip-strip stable.zip /var/www/lib/plugins/todo && \
  rm stable.zip

# add discussion plugin
  curl -o stable.zip -L "https://github.com/dokufreaks/plugin-discussion/archive/master.zip" && \
  unzip-strip stable.zip /var/www/lib/plugins/discussion && \
  rm stable.zip

# add edittable plugin
  curl -o stable.zip -L "https://github.com/cosmocode/edittable/archive/master.zip" && \
  unzip-strip stable.zip /var/www/lib/plugins/edittable && \
  rm stable.zip

# add explain plugin
  curl -o stable.zip -L "https://github.com/cosmocode/explain/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/explain && \
  rm stable.zip

# add inlinetoc plugin
  curl -o stable.zip -L "https://github.com/andreone/dokuwiki_inlinetoc/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/inlinetoc && \
  rm stable.zip

# add move plugin
  curl -o stable.zip -L "https://github.com/michitux/dokuwiki-plugin-move/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/move && \
  rm stable.zip

# add publish plugin
  curl -o stable.zip -L "https://github.com/cosmocode/dokuwiki-plugin-publish/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/publish && \
  rm stable.zip

# add tagging plugin
  curl -o stable.zip -L "https://github.com/cosmocode/tagging/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/tagging && \
  rm stable.zip

# add sqlite plugin
  curl -o stable.zip -L "https://github.com/cosmocode/sqlite/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/sqlite && \
  rm stable.zip

# add datatables plugin
  curl -o stable.zip -L "https://github.com/LotarProject/dokuwiki-plugin-datatables/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/datatables && \
  rm stable.zip

# add callflow plugin
  curl -o stable.zip -L "https://github.com/bojidar-bg/dokuwiki-callflow-plugin/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/callflow && \
  rm stable.zip

# add SwiftMail plugin
  curl -o stable.zip -L "https://github.com/splitbrain/dokuwiki-plugin-swiftmail/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/swiftmail && \
  rm stable.zip

# add konsole plugin
  curl -o stable.zip -L "https://github.com/chtiland/dokuwiki_plugin_konsole/archive/master.zip" && \
  unzip-strip stable.zip /var/www/lib/plugins/konsole && \
  rm stable.zip

# add cleanup plugin
  curl -o stable.zip -L "https://github.com/cosmocode/dokuwiki-plugin-cleanup/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/cleanup && \
  rm stable.zip

# add changes plugin
  curl -o stable.zip -L "https://github.com/cosmocode/changes/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/changes && \
  rm stable.zip

# add wrap plugin
  curl -o stable.zip -L "https://github.com/selfthinker/dokuwiki_plugin_wrap/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/wrap && \
  rm stable.zip

# add redirect2 plugin
  curl -o stable.zip -L "https://github.com/ssahara/dw-plugin-redirect2/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/redirect2 && \
  rm stable.zip

# add confmanager plugin
  curl -o stable.zip -L "https://github.com/cosmocode/confmanager/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/confmanager && \
  rm stable.zip

# add pagetitle plugin
  curl -o stable.zip -L "https://github.com/ssahara/dw-plugin-pagetitle/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/pagetitle && \
  rm stable.zip

# add include plugin
  curl -o stable.zip -L "https://github.com/dokufreaks/plugin-include/zipball/master" && \
  unzip-strip stable.zip /var/www/lib/plugins/include && \
  rm stable.zip

# add  plugin
  #curl -o stable.zip -L "" && \
  #unzip-strip stable.zip /var/www/lib/plugins/ && \
  #rm stable.zip
